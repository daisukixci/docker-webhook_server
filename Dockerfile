FROM python:3.10-alpine

ENV HOME=/home/app
ENV APP_HOME=/home/app/webhook

RUN addgroup --system app && adduser --system app

RUN apk add --no-cache build-base linux-headers pcre-dev

RUN mkdir -p $APP_HOME
COPY uwsgi.ini $APP_HOME
COPY requirements.txt $APP_HOME
COPY main.py ${APP_HOME}
RUN chown -R app:app $APP_HOME

WORKDIR $APP_HOME
USER app

ENV PATH="${HOME}/.local/bin/:${PATH}"
RUN python -m pip install --upgrade pip uwsgi
RUN python -m pip install --no-cache-dir -r requirements.txt

CMD uwsgi --ini ${APP_HOME}/uwsgi.ini
