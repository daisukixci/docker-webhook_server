# Docker Webhook Server
 Create a webhook server to forward Datadog alerts to Telegram.
 It relies on https://pypi.org/project/webhook-server/. The server will listen to port 8080
 so do not forget map the port 8080 with `-v HOST_PORT:8080`

## Secrets
For the container to start you need to pass:
- Authentication details (username/password)
- A valid Telegram Bot API key
You can pass them by using:
- Environment variables:
  - BASIC_AUTH_USERNAME
  - BASIC_AUTH_PASSWORD
  - TELEGRAM_BOT_API_TOKEN
- Docker Secrets:
  - basic_auth_username
  - basic_auth_password
  - telegram_bot_api_token

## Contribute
Open to any PR/comments/issues

## License
Copyright 2023 DaisukiXCI

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

