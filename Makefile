all:
ifndef VERSION
	$(error VERSION is undefined)
endif
	docker buildx build --platform linux/amd64 --build-arg webhook_server_version=${VERSION} -t webhook_server .
	docker tag webhook_server daisukixci/webhook_server
	docker tag webhook_server daisukixci/webhook_server:${VERSION}
	#docker push daisukixci/webhook_server
	#docker push daisukixci/webhook_server:${VERSION}
